#Sagui.rb
require_relative "produto"

class Ebook < Produto

  def matches?(query)
    ["ebook", "digital"].include?(query)
  end

  private 

    def calcula_preco(base)
       if @ano_lancamento < 2006
        if @possui_reimpressao
          base * 0.9
        else
          base * 0.95
        end
      elsif @ano_lancamento <= 2010
        base * 0.96
      else
        base
      end        
    end
end